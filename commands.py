# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import click
import csv
import datetime
import json
import os
import xmltodict

from ftfy import fix_encoding

from app import app, db
from helpers import (
    normalise_company_name,
    normalise_string,
    load_companies_index,
    load_persons_index,
)
from models import Company, Person, PersonCompany, Postcode, Tender

BULK_SIZE = 10_000
LOG_INTERVAL = 50_000
TEST_BREAK = None


@app.cli.command("import_ted")
@click.argument("path")
def import_ted(path):

    BULK_SIZE = 1_000
    LOG_INTERVAL = 1_000
    TEST_BREAK = None

    print("loading companies index...")
    companies_index = load_companies_index()
    print("done loading companies index.")

    count = 0
    from_germany = 0
    correct_authority_mappings = 0
    correct_contractor_mappings = 0
    # Iterate recursively through directory
    print("reading ted files…")
    for root, dirs, files in os.walk(path):
        records = []

        for file in files:
            if file.endswith(".xml"):
                with open(os.path.join(root, file)) as f:

                    count += 1

                    if TEST_BREAK and count > TEST_BREAK:
                        break

                    if count % BULK_SIZE == 0:
                        db.session.bulk_save_objects(records)
                        records = []

                    if count % LOG_INTERVAL == 0:
                        print(f"{count} records processed")
                        print(f"{from_germany} records from Germany")
                        print(
                            f"{correct_authority_mappings} correct authority mappings"
                        )
                        print(
                            f"{correct_contractor_mappings} correct contractor mappings"
                        )

                    record = xmltodict.parse(f.read())["TED_EXPORT"]

                    # TODO Include cases from outside Germany
                    if (not record["CODED_DATA_SECTION"]["NOTICE_DATA"]
                        ["ISO_COUNTRY"]["@VALUE"] == "DE"):
                        continue

                    country = "DE"
                    from_germany += 1

                    # Get the correct key nested under FORM_SECTION
                    form_keys = list(record["FORM_SECTION"].keys())
                    if len(form_keys) > 1:
                        form_key = form_keys[1]
                    else:
                        form_key = form_keys[0]

                    # TODO Include edge cases that break parsing now
                    if not form_key.startswith("F0"):
                        continue

                    # Tender title
                    try:
                        title = record["FORM_SECTION"][form_key][
                            "OBJECT_CONTRACT"]["TITLE"]["P"]
                    except TypeError:
                        try:
                            title = record["FORM_SECTION"][form_key][0][
                                "OBJECT_CONTRACT"]["TITLE"]["P"]
                        except KeyError:
                            title = record["CODED_DATA_SECTION"][
                                "NOTICE_DATA"]["NO_DOC_OJS"]
                    except KeyError:
                        title = record["CODED_DATA_SECTION"]["NOTICE_DATA"][
                            "NO_DOC_OJS"]

                    # Tender currency
                    try:
                        currency = record["CODED_DATA_SECTION"]["NOTICE_DATA"][
                            "VALUES"]["VALUE"]["@CURRENCY"]
                    except KeyError:
                        currency = None
                    except TypeError:
                        currency = None

                    # Tender value
                    try:
                        value = record["CODED_DATA_SECTION"]["NOTICE_DATA"][
                            "VALUES"]["VALUE"]["#text"]
                        value = float(value)
                    except KeyError:
                        value = None

                    # Tender URI
                    try:
                        url = record["CODED_DATA_SECTION"]["NOTICE_DATA"][
                            "URI_LIST"]["URI_DOC"]["#text"]
                    except:
                        try:
                            url = record["CODED_DATA_SECTION"]["NOTICE_DATA"][
                                "URI_LIST"]["URI_DOC"][0]["#text"]
                        except:
                            print(root + "/" + file)
                            url = None

                    # Tender publication date
                    try:
                        date = record["CODED_DATA_SECTION"]["REF_OJS"][
                            "DATE_PUB"]
                        parsed_date = datetime.datetime.strptime(
                            date, "%Y%m%d")
                        publication_date = parsed_date.date()
                    except KeyError:
                        print(root + "/" + file)
                        publication_date = None

                    # Tender deadline
                    try:
                        date = record["CODED_DATA_SECTION"]["CODIF_DATA"][
                            "DT_DATE_FOR_SUBMISSION"]
                        deadline = datetime.datetime.strptime(
                            date, "%Y%m%d %H:%M")
                    except ValueError:
                        date = record["CODED_DATA_SECTION"]["CODIF_DATA"][
                            "DT_DATE_FOR_SUBMISSION"]
                        deadline = datetime.datetime.strptime(date, "%Y%m%d")
                    except KeyError:
                        deadline = None

                    # TODO Add 'active' (True or False) attribute

                    # Tender contracting authority
                    try:
                        contracting_authority = record["FORM_SECTION"][
                            form_key]["CONTRACTING_BODY"][
                                "ADDRESS_CONTRACTING_BODY"]["OFFICIALNAME"]
                        normalised_authority_name = normalise_company_name(
                            contracting_authority)
                    except TypeError:
                        try:
                            contracting_authority = record["FORM_SECTION"][
                                form_key][0]["CONTRACTING_BODY"][
                                    "ADDRESS_CONTRACTING_BODY"]["OFFICIALNAME"]
                            normalised_authority_name = normalise_company_name(
                                contracting_authority)
                        except KeyError:
                            normalised_authority_name = None
                    except KeyError:
                        normalised_authority_name = None

                    if normalised_authority_name not in companies_index:
                        continue

                    authority_id = companies_index[normalised_authority_name]
                    correct_authority_mappings += 1

                    # Tender contractor
                    try:
                        contractor = record["FORM_SECTION"][form_key][
                            "AWARD_CONTRACT"]["AWARDED_CONTRACT"][
                                "CONTRACTORS"]["CONTRACTOR"][
                                    "ADDRESS_CONTRACTOR"]["OFFICIALNAME"]
                        normalised_contractor_name = normalise_company_name(
                            contractor)
                    except:
                        normalised_contractor_name = None

                    if normalised_contractor_name in companies_index:
                        contractor_id = companies_index[
                            normalised_contractor_name]
                        correct_contractor_mappings += 1
                    else:
                        contractor_id = None

                    try:  # TODO Improve error handling
                        tender = Tender(
                            title=title[:300],
                            country=country,
                            url=url,
                            currency=currency,
                            value=value,
                            publication_date=publication_date,
                            deadline=deadline,
                            contracting_authority_id=authority_id,
                            winner_id=contractor_id,
                        )
                        records.append(tender)
                    except Exception as e:
                        print(root + "/" + file)
                        print(record)
                        print(e)
                        continue

        db.session.bulk_save_objects(records)
        records = []
        db.session.commit()

    print("done reading tenders")


@app.cli.command("import_postcodes")
@click.argument("path")
def import_postcodes(path):
    print("reading postcodes file…")

    with open(path, "r") as file:
        reader = csv.reader(file, delimiter=";")

        for line in reader:
            csv_postcode = line[0].zfill(5)
            csv_name = fix_encoding(line[1])
            csv_coordinates = f"POINT({line[2]} {line[3]})"

            postcode = Postcode.query.filter(
                Postcode.postcode == csv_postcode).first()

            if not postcode:
                postcode = Postcode(postcode=csv_postcode,
                                    name=csv_name,
                                    coordinates=csv_coordinates)
                db.session.add(postcode)

    db.session.commit()
    print("done ᕕ( ᐛ )ᕗ")


@app.cli.command("import_open_corporates")
@click.argument("path")
def import_open_corporates(path):
    print("reading open corporates file…")
    print("reading companies…")

    with open(path, "r") as file:

        count = 0
        records = []

        for line in file:

            count += 1

            if TEST_BREAK and count > TEST_BREAK:
                break

            if count % BULK_SIZE == 0:
                db.session.bulk_save_objects(records)
                records = []

            if count % LOG_INTERVAL == 0:
                print(f"{count} records processed")

            record = json.loads(line)

            json_name = record.get("name", None)
            all_attributes = record.get("all_attributes", {})
            json_register_art = all_attributes.get("_registerArt", None)
            json_number = all_attributes.get("native_company_number", None)

            if json_register_art != "HRB":
                continue

            if not json_name:
                print("skipping unknown company")
                continue

            company = Company(name=json_name[:200], number=json_number)
            records.append(company)

        db.session.bulk_save_objects(records)
        records = []
        db.session.commit()

    print("done reading companies")
    print("reading persons…")

    with open(path, "r") as file:

        count = 0
        records = []
        seen = {}

        for line in file:

            count += 1

            if TEST_BREAK and count > TEST_BREAK:
                break

            if count % BULK_SIZE == 0:
                db.session.bulk_save_objects(records)
                records = []

            if count % LOG_INTERVAL == 0:
                print(f"{count} records processed")

            record = json.loads(line)

            officers = record.get("officers", [])

            for officer in officers:
                json_name = officer.get("name", None)
                json_type = officer.get("type", None)

                if not json_name or json_type != "person":
                    continue

                normalised_name = normalise_string(json_name)

                if seen.get(normalised_name, True):
                    seen[normalised_name] = False
                    person = Person(name=json_name[:200])
                    records.append(person)

        db.session.bulk_save_objects(records)
        records = []
        del seen
        db.session.commit()

    print("done reading persons")
    print("reading company - person relations…")

    companies_index = load_companies_index()
    persons_index = load_persons_index()

    with open(path, "r") as file:

        count = 0
        records = []
        existing = {}

        for line in file:

            count += 1

            if TEST_BREAK and count > TEST_BREAK:
                break

            if count % (BULK_SIZE * 5) == 0:
                # increased bulk size because the model only contains int values
                db.session.bulk_save_objects(records)
                records = []

            if count % LOG_INTERVAL == 0:
                print(f"{count} records processed")

            record = json.loads(line)
            json_name = record.get("name", None)
            normalised_company_name = normalise_company_name(json_name)

            if normalised_company_name not in companies_index:
                continue

            officers = record.get("officers", [])

            for officer in officers:
                json_person_name = officer.get("name", None)
                json_person_type = officer.get("type", None)

                if not json_person_name or json_person_type != "person":
                    continue

                normalised_person_name = normalise_string(json_person_name)

                if normalised_person_name not in persons_index:
                    continue

                company_id = companies_index[normalised_company_name]
                person_id = persons_index[normalised_person_name]

                if existing.get((person_id, company_id), True):
                    existing[(person_id, company_id)] = False
                    person_company = PersonCompany(person_id=person_id,
                                                   company_id=company_id)
                    records.append(person_company)

        db.session.bulk_save_objects(records)
        records = []
        db.session.commit()

    print("done reading company - person relations")
    print("all done ᕕ( ᐛ )ᕗ")

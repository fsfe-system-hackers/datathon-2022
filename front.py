# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from millify import millify, prettify
import streamlit as st
import pandas as pd
import requests
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from app import app
from models import Tender, Company, Person, PersonCompany, Postcode

if not os.getenv('BACKEND_URL'):
    BACKEND_URL = "http://localhost:5000"
else:
    BACKEND_URL = os.getenv('BACKEND_URL')

st.set_page_config(page_title="FSFE EU Datathon", page_icon="🇪🇺")


@st.experimental_singleton
def get_db_sessionmaker():
    # Get the DB sessionmaker object and cache it for every session
    if not os.getenv('DATABASE_URL'):
        DB_URI = "postgresql://postgres:geheim@localhost:13339/postgres"
    else:
        DB_URI = f"postgresql://postgres:geheim@{os.getenv('DATABASE_URL')}/postgres"
    engine = create_engine(DB_URI)
    return sessionmaker(engine)


db = get_db_sessionmaker()


@st.experimental_memo
def get_all(_sessionmaker, model):
    """Retrieve rows from the database, and cache them.

    Parameters
    ----------
    _sessionmaker : a SQLAlchemy session factory. Because this arg name is
                    prefixed with "_", it won't be hashed.
    model : the SQLAlchemy model

    Returns
    -------
    pandas.DataFrame
    A DataFrame containing the retrieved rows. Mutating it won't affect
    the cache.
    """
    with _sessionmaker() as session:
        query = (session.query(model).order_by(model.id))

        return pd.read_sql(query.statement, query.session.bind)


@st.experimental_memo()
def get_page(_sessionmaker, model, page_size, page):
    """Retrieve rows from the database, and cache them.

    Parameters
    ----------
    _sessionmaker : a SQLAlchemy session factory. Because this arg name is
                    prefixed with "_", it won't be hashed.
    model : the SQLAlchemy model
    page_size : the number of rows in a page of result
    page : the page number to retrieve

    Returns
    -------
    pandas.DataFrame
    A DataFrame containing the retrieved rows. Mutating it won't affect
    the cache.
    """
    with _sessionmaker() as session:
        query = (session.query(model).offset(page_size *
                                             page).limit(page_size))

        return pd.read_sql(query.statement, query.session.bind)


@st.experimental_memo()
def get_row_count(_sessionmaker, model):
    """Retrieve rows from the database, and cache them.

    Parameters
    ----------
    _sessionmaker : a SQLAlchemy session factory. Because this arg name is
                    prefixed with "_", it won't be hashed.
    model : the SQLAlchemy model

    Returns
    -------
    pandas.DataFrame
    A DataFrame containing the retrieved rows. Mutating it won't affect
    the cache.
    """
    with _sessionmaker() as session:
        rows = session.query(model).count()
        return rows


@st.cache
def get_api(route, query=None):
    if query is None:
        url = f"{BACKEND_URL}/{route}"
    else:
        url = f"{BACKEND_URL}/{route}?{query}"
    response = requests.get(url)
    print(f"GET {url} {response.status_code}")
    return response.json()


st.write('# EU Datathon 2022')
st.write(
    '#### Application prototype by the Free Software Foundation Europe e.V.')
st.write('''
This is a prototype demonstration of our contribution to the EU datathon. It
showcases, albeit rudimentarily, the core functionality that has already been
implemented at this early stage of the datathon. All the code of this prototype
is licensed under the GPL-3.0 license,
[reuse-compliant](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/datathon-2022)
and available [here](https://git.fsfe.org/fsfe-system-hackers/datathon-2022).

#### What is included in the prototype?
We have successfully integrated the [TED dataset](https://ted.europa.eu) with
the [German OpenCorporates dataset](https://offeneregister.de/) to enable users
to see which companies and legal persons are involved in any given tender. This
connection between publicly available tendering and company data is the original
contribution of this app.  This prototype is a first attempt at showing what
types of queries this connection of data sources might make possible.
''')

col1, col2, col3, = st.columns(3)
with col1:
    st.metric("TED notices (2022/DE only)",
              millify(get_row_count(db, Tender), precision=2))
with col2:
    st.metric("Companies", millify(get_row_count(db, Company), precision=2))
with col3:
    st.metric("Persons", millify(get_row_count(db, Person), precision=2))

st.write('''
#### Limitations
- Only tenders published in January and February 2022 are considered.
- Only tenders from Germany are considered.
- The data has not been fully cleaned or translated and might have been parsed
  erroneously.
- Database access via the API is not yet publicly available and serves
  demonstration purposes only.
''')

st.write('## Our API')
st.write('''
Plase note that, for now, the `curl` statements below are for illustration
purposes only. For security reasons, the database and the API are not yet
publicly available.
''')
st.write('### Special queries')
st.write('''
Let's see which (German) companies have tendered the most in 2022.
''')
st.code(f'curl {BACKEND_URL}/queries/companies_highest_tender_value_sum',
        language='bash')
if st.button('Run query', key=1):
    st.json(get_api("queries/companies_highest_tender_value_sum"))

st.write('''
Let's now see which (German) persons are involved with the most companies.
''')
st.code(f'curl {BACKEND_URL}/queries/persons_most_companies', language='bash')
if st.button('Run query', key=2):
    st.json(get_api("queries/persons_most_companies"))

st.write('### Search by tender title')
st.write('''
This is one way in which an investigation into the EU public procurement process
might start
''')
st.code(f'curl {BACKEND_URL}/tenders?name=<string>', language='bash')
tender_title_query = st.text_input('Tender title:', "Microsoft")
st.json(get_api("tenders", f"title={tender_title_query}"))

st.write('### Search by tender ID')
st.write('''
''')
st.code(f'curl {BACKEND_URL}/tenders/<id>', language='bash')
tender_id_query = st.text_input('Tender ID:', "")
if tender_id_query:
    st.json(get_api(f"tenders/{tender_id_query}"))

st.write('### Search by company name')
st.write('''
Here, you search for companies and figure out their ID, which can then be used
to search the tender database for tenders published by company, contracts the
company won and individuals associated with the company.
''')
st.code(f'curl {BACKEND_URL}/company?name=<string>', language='bash')
search_query = st.text_input('Company name:', "Crayon")
st.json(get_api("companies", f"name={search_query}"))

st.write('### Search by company ID')
st.code(f'curl {BACKEND_URL}/company/<id>', language='bash')
company_id_query = st.text_input('Company ID', "546480")
if company_id_query:
    st.json(get_api(f"companies/{company_id_query}"))

st.write("## The Data Model")
st.write("""
The three tables below give you an overview of the data model in its current
rudimentary state. The view is limited to 500 rows. We are planning on a full
implementation of the [Open Contracting Data
Standard](https://standard.open-contracting.org/profiles/eu/latest/en/) for TED
data in the future. In addition, we are already working on adding more openly
available data on companies and legal persons associated with companies to our
database.
""")
st.write("### Tenders `Tender`")
tenders_dtypes_dict = {
    'id': "Int64",
    'contracting_authority_id': "Int64",
    'winner_id': "Int64",
}
st.write(get_page(db, Tender, 500, 0).astype(tenders_dtypes_dict))
st.write("### Companies `Tender`")
st.write(get_page(db, Company, 500, 0))
st.write("### Persons `Person`")
st.write(get_page(db, Person, 500, 0))
st.write("### Person 🤝 Company `PersonCompany`")
st.write(get_page(db, PersonCompany, 500, 0))

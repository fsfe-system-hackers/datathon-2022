# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import re


def normalise_string(value):
    value = str(value)
    value = value.casefold()
    value = value.replace(u"ä", "ae")
    value = value.replace(u"ö", "oe")
    value = value.replace(u"ü", "ue")
    value = value.replace(u"ß", "ss")
    value = re.sub("[^a-z0-9]+", "", value)
    return value


def normalise_company_name(value):
    value = normalise_string(value)
    value = value.replace("aktiengesellschaft", "ag")
    value = value.replace("gesellschaftmitbegrenzterhaftung", "gmbh")
    return value


def load_companies_index():
    """
    Returns a dict: normalised company name => company DB ID
    """
    from app import db
    from models import Company

    companies = db.session.query(Company.id, Company.name).all()
    index = {}

    for (company_id, name) in companies:
        index[normalise_company_name(name)] = company_id

    return index


def load_persons_index():
    """
    Returns a dict: normalised person name => person DB ID
    """
    from app import db
    from models import Person

    persons = db.session.query(Person.id, Person.name).all()
    index = {}

    for (person_id, name) in persons:
        index[normalise_string(name)] = person_id

    return index

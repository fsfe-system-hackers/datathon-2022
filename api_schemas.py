# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from app import ma
from models import Company, Person, Tender


class CompanyApiSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Company

    id = ma.auto_field()
    name = ma.auto_field()


class PersonApiSchema(ma.SQLAlchemySchema):

    class Meta:
        model = Person

    id = ma.auto_field()
    name = ma.auto_field()


class TenderApiSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = Tender

        id = ma.auto_field()
        title = ma.auto_field()

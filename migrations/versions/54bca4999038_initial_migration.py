# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: CC0-1.0

"""Initial migration.

Revision ID: 54bca4999038
Revises:
Create Date: 2022-03-28 22:37:52.869534

"""
from alembic import op
import geoalchemy2
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "54bca4999038"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "company",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=200), nullable=False),
        sa.Column("number", sa.String(length=100), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_company_number"),
                    "company", ["number"],
                    unique=False)
    op.create_table(
        "person",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(length=200), nullable=False),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_person_name"), "person", ["name"], unique=False)
    op.create_table(
        "postcode",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("postcode", sa.String(length=5), nullable=False),
        sa.Column("name", sa.String(length=100), nullable=False),
        sa.Column(
            "coordinates",
            geoalchemy2.types.Geometry(geometry_type="POINT",
                                       from_text="ST_GeomFromEWKT",
                                       name="geometry"),
            nullable=True,
        ),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("postcode"),
    )
    op.create_table(
        "person_company",
        sa.Column("person_id", sa.Integer(), nullable=False),
        sa.Column("company_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["company_id"],
            ["company.id"],
        ),
        sa.ForeignKeyConstraint(
            ["person_id"],
            ["person.id"],
        ),
        sa.PrimaryKeyConstraint("person_id", "company_id"),
    )


def downgrade():
    op.drop_table("person_company")
    op.drop_table("postcode")
    op.drop_index(op.f("ix_person_name"), table_name="person")
    op.drop_table("person")
    op.drop_index(op.f("ix_company_number"), table_name="company")
    op.drop_table("company")

# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: CC0-1.0

{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  buildInputs = [ wget pipenv bzip2 gcc zlib reuse ];
  LD_LIBRARY_PATH = "${gcc.cc.lib}/lib:${pkgs.zlib}/lib";
}

# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: CC0-1.0

FROM debian:bullseye-slim

WORKDIR /app

RUN apt-get update
RUN apt-get install -y pipenv
COPY Pipfile Pipfile.lock ./
RUN pipenv install --system --deploy

ENV FLASK_APP=app
ENV FLASK_ENV=development

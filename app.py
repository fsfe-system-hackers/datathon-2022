# SPDX-FileCopyrightText: 2022 FSFE System Hackers <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app = Flask(__name__)

if not os.getenv('DATABASE_URL'):
    app.config[
        "SQLALCHEMY_DATABASE_URI"] = "postgresql://postgres:geheim@localhost:13339/postgres"
else:
    app.config[
        "SQLALCHEMY_DATABASE_URI"] = f"postgresql://postgres:geheim@{os.getenv('DATABASE_URL')}/postgres"

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db, compare_type=True)
ma = Marshmallow(app)

import commands  # noqa
import routes  # noqa
